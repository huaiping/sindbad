### Sindbad CMS  
[![license](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat)](https://gitlab.com/huaiping/sindbad/blob/master/LICENSE) [![Build Status](https://travis-ci.com/huaiping/sindbad.svg?branch=master)](https://travis-ci.com/huaiping/sindbad) [![Coverage Status](https://coveralls.io/repos/github/huaiping/sindbad/badge.svg?branch=master)](https://coveralls.io/github/huaiping/sindbad?branch=master)  
Open Source CMS. Just for practice.

### Requirements
Node.js 10.15+ [https://nodejs.org](https://nodejs.org)  
MySQL 5.5+ [https://www.mysql.com](https://www.mysql.com)

### Features
HTML 5 + Express 4.16.4 + Bootstrap 3.4.0

### Installation
```
$ git clone https://github.com/huaiping/sindbad.git
$ cd sindbad
$ npm install --registry https://registry.npm.taobao.org
$ npm start
```

### Demo
[https://www.huaiping.net](https://huaiping.net/v2/)

### License
Licensed under the [MIT License](https://gitlab.com/huaiping/sindbad/blob/master/LICENSE).
