var express = require('express');
var mysql = require('mysql');
var moment = require('moment');
var crypto = require('crypto');
var router = express.Router();

var pool = mysql.createPool({
    host: 'qdm160638220.my3w.com',
    user: 'qdm160638220',
    password: '',
    database: 'qdm160638220_db',
    port: 3306,
    multipleStatements: true,
    dateStrings: true
});

/* index */
router.get('/', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_news WHERE status="passed" ORDER BY pubtime DESC limit 10; SELECT * FROM swan_links WHERE status="passed";', function(err, result){
            result[0].forEach(function(latest) {
                latest.pubtime = moment(latest.pubtime).format('YYYY-MM-DD');
            });
            res.render('home_lists', { latest:result[0], links:result[1] });
            conn.release();
        });
    });
});

/* news */
router.get('/news', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_news WHERE status="passed" ORDER BY pubtime DESC limit 20;', function(err, news){
            res.render('news_lists', { news:news });
            conn.release();
        });
    });
});

/* news-pages */
router.get('/news/page', function(req, res, next) {
    res.render('news_pages', { title: 'login' });
});

/* news-view */
router.get('/news/view/:id', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_news WHERE id=' + req.params.id, function(err, news){
            var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
            res.render('news_items', { news:news, url:fullUrl });
            conn.release();
        });
    });
});

/* photo */
router.get('/photo', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT id, category FROM swan_photo WHERE status="passed" GROUP BY category; SELECT * FROM swan_photo GROUP BY subcategory', function(err, result){
            res.render('photo_lists', { categories:result[0], subcategories:result[1] });
            conn.release();
        });
    });
});

/* photo-view */
router.get('/photo/view/:id', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_photo WHERE subcategory=(SELECT subcategory FROM swan_photo WHERE id=' + req.params.id + ')', function(err, photos){
            res.render('photo_items', { photos:photos });
            conn.release();
        });
    });
});

/* photo-waterfall */
router.get('/photo/waterfall', function(req, res, next) {
    res.render('photo_waterfall', { title: 'login' });
});

/* download */
router.get('/download', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_download WHERE status="passed" ORDER BY hits DESC limit 15; SELECT * FROM swan_download WHERE status="passed" ORDER BY pubtime DESC;', function(err, result){
            res.render('down_lists', { down_hots:result[0], down_lists:result[1] });
            conn.release();
        });
    });
});

/* download-view */
router.get('/download/view/:id', function(req, res) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_download WHERE status="passed" ORDER BY hits DESC; SELECT * FROM swan_download WHERE id=' + req.params.id + ';', function(err, result){
            res.render('down_items', { down_hots:result[0], down_item:result[1] });
            conn.release();
        });
    });
});

/* download-mirrors */
router.get('/download/mirrors', function(req, res, next) {
    res.redirect('/membership');
});

/* guestbook */
router.get('/guestbook', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_guestbook WHERE status="passed" ORDER BY pubtime DESC', function(err, messages){
            res.render('guestbook', { messages:messages });
            conn.release();
        });
    });
});

/* guestbook-message */
router.get('/guestbook/message', function(req, res, next) {
    res.redirect('/membership');
});

/* video */
router.get('/video', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_video WHERE id=1; SELECT * FROM swan_video WHERE status="passed" ORDER BY hits DESC; SELECT * FROM swan_video WHERE category="lecture" and status="passed" ORDER BY hits DESC', function(err, result){
            res.render('video', { video_item:result[0], video_hots:result[1], video_lectures:result[2] });
            conn.release();
        });
    });
});

/* video-view */
router.get('/video/view/:id', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_video WHERE status="passed" ORDER BY hits DESC; SELECT * FROM swan_video WHERE id=' + req.params.id + '; SELECT * FROM swan_video WHERE category="lecture" and status="passed" ORDER BY hits DESC', function(err, result){
            res.render('video', { video_hots:result[0], video_item:result[1], video_lectures:result[2] });
            conn.release();
        });
    });
});

/* membership */
router.get('/membership', function(req, res, next) {
    res.render('login', { title: 'login' });
});

/* membership-login */
router.post('/membership/login', function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    var session = req.session;
    var sha1 = crypto.createHash('sha1');
    password = sha1.update(password).digest('hex');
    var md5 = crypto.createHash('md5');
    password = md5.update(password).digest('hex');
    //res.render('login', { title: 'login', user:user_name, pass:password });
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_membership WHERE username="' + username + '" AND password="' + password + '";', function(err, data){
            //res.render('photo_items', { photos:photos });
            req.session = data;
            console.log(req.session[0].name);
            conn.release();
        });
    });
});

/* membership-logout */
router.get('/membership/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/');
});

/* score */
router.get('/score', function(req, res, next) {
    res.render('score', { title: 'login' });
});

/* score-view */
router.post('/score/view', function(req, res) {
    var category = req.body.category;
    var name = req.body.name;
    var number = req.body.number;
    var strlen = number.length;
    var cats = category.split("|");
    if (cats[1]) {
        sql = 'FROM swan_score WHERE item in (' + cats[0] + ', ' + cats[1] + ')';
    } else {
        sql = 'FROM swan_score WHERE item=' + category;
    }
    var fullUrl = req.protocol + '://' + req.get('host') + '/score';
    pool.getConnection(function (err, conn) {
        if (strlen == 6) {
            conn.query('SELECT * ' + sql + ' AND xh=' + number + ' AND xm=' + pool.escape(name) + '; SELECT max(jm) AS jm ' + sql + ';', function(err, scores){
                res.render('score_detail', { score:scores[0], total:scores[1], strlen:strlen, url:fullUrl });
                conn.release();
            });
        } else if (strlen == 18) {
            conn.query('SELECT * ' + sql + ' AND sfzh=' + number + ' AND xm=' + pool.escape(name) + '; SELECT max(jm) AS jm ' + sql + '; SELECT * FROM swan_score WHERE sfzh=' + number + ';', function(err, scores){
                res.render('score_detail', { score:scores[0], total:scores[1], scores:scores[2], strlen:strlen, url:fullUrl });
                conn.release();
            });
        } else {
            conn.query('SELECT * ' + sql + ' AND bj=' + number + ' AND xm=' + pool.escape(name) + '; SELECT max(jm) AS jm ' + sql + ';', function(err, scores){
                res.render('score_detail', { score:scores[0], total:scores[1], strlen:strlen, url:fullUrl });
                conn.release();
            });
        }
    });
});

/* survey */
router.get('/survey', function(req, res, next) {
    res.redirect('/membership');
});

/* exam */
router.get('/exam', function(req, res, next) {
    res.redirect('/membership');
});

/* salary */
router.get('/salary', function(req, res, next) {
    res.redirect('/membership');
});

/* repairs */
router.get('/repairs', function(req, res, next) {
    pool.getConnection(function (err, conn) {
        conn.query('SELECT * FROM swan_repairs ORDER BY pubtime DESC', function(err, lists){
            lists.forEach(function(list) {
                list.pubtime = moment(list.pubtime).format('YYYY-MM-DD');
            });
            res.render('repairs_lists', { repairs_lists:lists });
            conn.release();
        });
    });
});

/* repairs-report */
router.get('/repairs/report', function(req, res, next) {
    res.render('repairs', { title: 'login' });
});

/* repairs-post */
router.post('/repairs/post', function(req, res, next) {
    var category = req.body.category;
    var department = req.body.department;
    var item = req.body.item;
    var address = req.body.address;
    var details = req.body.details;
    var customer = req.body.customer;
    var telephone = req.body.telephone;
    var pubtime = moment(Date.now()).format('YYYY-MM-DD H:m:s');
    pool.getConnection(function (err, conn) {
        conn.query('INSERT INTO swan_repairs(category, department, item, address, details, customer, telephone, pubtime, status) VALUES ("'+category+'", "'+department+'", "'+item+'", "'+address+'", "'+details+'", "'+customer+'", "'+telephone+'", "'+pubtime+'", "pending");', function(err, result){
            conn.release();
        });
    });
    res.redirect('/repairs');
});

module.exports = router;
